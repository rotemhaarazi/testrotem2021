import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BooksService {

  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  public getBooks(userId,startAfter){
    this.bookCollection = this.db.collection(`users/${userId}/books`, 
     ref => ref.orderBy('title','asc').limit(4).startAfter(startAfter));
    return this.bookCollection.snapshotChanges() 
    }

  public deleteBook(Userid:string, id:string){
    this.db.doc(`users/${Userid}/books/${id}`).delete();
  }
  
  updateBook(userId:string,id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        title:title,
        author:author
      }
    )
  }

  addBook(userId:string, title:string, author:string,){
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection('books').add(book);
  }

/*
   public getBooks(userId):Observable<any[]> {
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(map(
      collection => collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))

     }
*/


  constructor(private db:AngularFirestore) { }
}
